const Heading = ({ children }) => (
  <h1 className="text-2xl sm:text-6xl font-bold">{children}</h1>
);

export default Heading;
